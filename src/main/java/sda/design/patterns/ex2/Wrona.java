package sda.design.patterns.ex2;

public class Wrona {
    private static int index = 1;
    private int wronaNo;

    public Wrona() {
        wronaNo = index;
        index++;

    }

    public String krakanie() {
        return "Nr wrona:" + wronaNo + "Kra! kra!";
    }
}
