package sda.design.patterns.ex2;

public class AdapterWronaToDog extends Dog {

    private Wrona wrona;

    public AdapterWronaToDog(Wrona wrona) {
        this.wrona = wrona;
    }



    public String woof() {return wrona.krakanie();}

}
